package br.marcus.pattern.dao;

import java.util.ArrayList;
import java.util.List;

/**
 * In Memory Data access implementation. 
 * 
 * @author marcusmazzo
 *
 */
public class InMemoryDao implements Dao{

	public static List<CarEntity> listInMemory = new ArrayList<>();

	@Override
	public void insert(CarEntity object) {
		listInMemory.add(object);
	}

	@Override
	public void update(CarEntity object) {
		listInMemory.remove(listInMemory.indexOf(object));
		listInMemory.add(object);
	}

	@Override
	public List<CarEntity> list() {
		return listInMemory;
	}

	@Override
	public void delete(CarEntity object) {
		listInMemory.remove(listInMemory.indexOf(object));
	}
	
}
