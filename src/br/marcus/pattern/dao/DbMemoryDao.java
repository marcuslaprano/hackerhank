package br.marcus.pattern.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.h2.jdbcx.JdbcDataSource;

/**
 * Database Implementation of DAO pattern.
 * @author marcusmazzo
 *
 */
public class DbMemoryDao implements Dao {

	private static final String DB_URL = "jdbc:h2:~/dao";
	private static final String CREATE_SCHEMA_SQL = "CREATE TABLE CARS (ID NUMBER, CARNAME VARCHAR(100))";
	private static final String DROP_SCHEMA_SQL = "DROP TABLE CARS";
	private static final String INSERT = "INSERT INTO CARS(id, carname) values(?,?)";
	private static final String UPDATE = "UPDATE CARS SET CARNAME =  ? WHERE ID = ?";
	private static final String DELETE = "DELETE FROM CARS WHERE ID = ?";
	private static final String LIST = "Select id, carname from cars";
	private static final String MAX_ID = "select max(id) max_id from cars";

	private DataSource dataSource;

	public DbMemoryDao() {
		dataSource = new JdbcDataSource();
		((JdbcDataSource) dataSource).setURL(DB_URL);
		Connection connection;
		try {
			try {
				removeSchema();
			} catch (Exception e) {
				// TODO: handle exception
			}
			connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			statement.execute(CREATE_SCHEMA_SQL);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void insert(CarEntity object) {
		try {
			Statement stmt = dataSource.getConnection().createStatement();
			ResultSet rs = stmt.executeQuery(MAX_ID);
			Long id = 0L;
			while (rs.next()) {
				id = rs.getLong("max_id");
				id++;
			}

			PreparedStatement pstmt = dataSource.getConnection().prepareStatement(INSERT);
			pstmt.setLong(1, id);
			pstmt.setString(2, object.getCarName());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void update(CarEntity object) {
		try {
			PreparedStatement stmt = dataSource.getConnection().prepareStatement(UPDATE);
			stmt.setString(1, object.getCarName());
			stmt.setLong(2, object.getId());
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public List<CarEntity> list() {
		List<CarEntity> list = new ArrayList<>();
		try {
			Statement stmt = dataSource.getConnection().createStatement();
			ResultSet rs = stmt.executeQuery(LIST);
			while (rs.next()) {
				CarEntity car = new CarEntity();
				car.setId(rs.getLong("id"));
				car.setCarName(rs.getString("carname"));
				list.add(car);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public void delete(CarEntity object) {
		try {
			PreparedStatement stmt = dataSource.getConnection().prepareStatement(DELETE);
			stmt.setLong(1, object.getId());
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void removeSchema() {
		try {
			Connection connection = dataSource.getConnection();
			Statement statement = connection.createStatement();
			statement.execute(DROP_SCHEMA_SQL);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
