package br.marcus.pattern.dao;

import java.util.ArrayList;

/**
 * Main class to perform DAO pattern.
 * In this class we demonstrate how to implement a DAO pattern. DAO Pattern must abstract implementation of data access. 
 * On first step we use an InMemory database, an {@link ArrayList}.
 * On second step we use a H2 database with database insert.
 * 
 * We can note that Main.class don't know how insert method is implemented. So we can switch easily
 * from one database to another without generate impact on business layer.
 * 
 * @author marcusmazzo
 *
 */
public class Main {
	
	private Dao dao;

	public Main() {
		dao = new InMemoryDao();
		doSomething(dao);
		
		dao = new DbMemoryDao();
		doSomething(dao);
		
	}

	private void doSomething(Dao dao) {
		CarEntity car = new CarEntity();
		dao.insert(car);
		for(CarEntity carEntity: dao.list()) {
			System.out.println(carEntity.getId() + " - " + carEntity.getCarName());
		}
		
	}
	
	public static void main(String[] args) {
		new Main();
	}

}
