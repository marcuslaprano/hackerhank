package br.marcus.pattern.dao;

import java.util.List;

/**
 * Dao interface that provides basic methods.
 * @author marcusmazzo
 *
 */
public interface Dao {

	void insert(CarEntity object);
	void update(CarEntity object);
	List<CarEntity> list();
	void delete(CarEntity object);

}
