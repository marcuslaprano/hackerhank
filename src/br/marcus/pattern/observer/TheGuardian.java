package br.marcus.pattern.observer;

import java.util.Observable;

public class TheGuardian implements JournalObservable{

	@Override
	public void doSomething(Journal journal) {
		System.out.println("The journal The Guardian informs "+journal.getMessage());
	}

	@Override
	public void update(Observable journalObservable, Object journal) {
		if(journalObservable instanceof JournalObservableImpl) {
			doSomething((Journal) journal);
		}
	}

}
