package br.marcus.pattern.observer;

import java.util.Observer;

public interface JournalObservable extends Observer{
	
	void doSomething(Journal journal);

}
