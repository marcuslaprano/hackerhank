package br.marcus.pattern.observer;

/**
 * Main.java
 * This program simulates the design pattern Observer.
 * 
 * In this pattern create an interface that provides access to observer.
 * We create a class that Extends observable structure and we register ours
 * observers in our subject.
 * 
 * After this register we execute the method notifyObservers and every registered
 * observer will be notified.
 * 
 * 
 *
 * 
 * @author marcusmazzo
 *
 */
public class Main {
	
	public static void main(String[] args) {
		JournalObservableImpl observer = new JournalObservableImpl();
		
		Journal journal = new Journal();
		journal.setMessage("New Message from President was delivered");
		observer.notifyObservers(journal);
		
	}

}
