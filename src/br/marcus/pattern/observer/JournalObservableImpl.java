package br.marcus.pattern.observer;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class JournalObservableImpl extends Observable{
	
	private List<Observer> observers = new ArrayList<>();
	
	public JournalObservableImpl() {
		registerListener();
	}
	
	public void registerListener() {
		TheDayle theDayle = new TheDayle();
		TheGuardian theGuardian = new TheGuardian();
		observers.add(theGuardian);
		observers.add(theDayle);
	}
	
	public void notifyObservers(Journal journal) {
		for(Observer observer: observers){
			observer.update(this, journal);
		}
		observers.clear();
	}

}
