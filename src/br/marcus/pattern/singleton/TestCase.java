package br.marcus.pattern.singleton;

public class TestCase {
	
	public static void main(String[] args) {
		Singleton sin = Singleton.getSingleton();
		System.out.println(sin.toString());
		
		Singleton sin2 = Singleton.getSingleton();
		System.out.println(sin2.toString());
		
		Singleton sin3 = Singleton.getSingleton();
		System.out.println(sin3.toString());
	}

}
