package br.marcus.hackerhank.algorithms.gradingstudents;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Solution {

    /*
     * Complete the gradingStudents function below.
     */
    static int[] gradingStudents(int[] grades) {
    	List<Integer> list = new ArrayList<>();
    	for(int grade: grades) {
    		if(grade < 38) {
    			list.add(grade);
    		} else {
	    		Integer test = grade % 5;
	    		test = grade - test + 5;
	    		if(test - grade >= 3) {
	    			list.add(grade);
	    		} else {
	    			list.add(test); 
	    		}
    		}
    		
    	}
    	
    	return list.stream().mapToInt(Integer::intValue).toArray();

    }

    private static final Scanner scan = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
    	BufferedWriter bw = new BufferedWriter(
				new FileWriter(new File("C:\\Users\\marcusmazzo\\Desktop\\tmp.txt")));
        int n = Integer.parseInt(scan.nextLine().trim());

        int[] grades = new int[n];

        for (int gradesItr = 0; gradesItr < n; gradesItr++) {
            int gradesItem = Integer.parseInt(scan.nextLine().trim());
            grades[gradesItr] = gradesItem;
        }

        int[] result = gradingStudents(grades);

        for (int resultItr = 0; resultItr < result.length; resultItr++) {
            bw.write(String.valueOf(result[resultItr]));

            if (resultItr != result.length - 1) {
                bw.write("\n");
            }
        }

        bw.newLine();

        bw.close();
    }
}
