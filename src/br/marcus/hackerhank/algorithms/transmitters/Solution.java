package br.marcus.hackerhank.algorithms.transmitters;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Solution {
	static int hackerlandRadioTransmitters(int[] x, int k) {
		Arrays.sort(x);
		int trans = 0;
		int i = 0;
		int n = x.length;
		while (i < n) {
			trans++;
			int loc = x[i] + k;
			while (i < n && x[i] <= loc)
				i++;
			loc = x[--i] + k;
			while (i < n && x[i] <= loc)
				i++;
		}

		return trans; 
	}

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
		BufferedWriter bufferedWriter = new BufferedWriter(
				new FileWriter(new File("C:\\Users\\marcusmazzo\\Desktop\\tmp.txt")));
		String[] nk = scanner.nextLine().split(",");
		int n = Integer.parseInt(nk[0]);
		int k = Integer.parseInt(nk[1]);
		int[] x = new int[n];
		String[] xItems = scanner.nextLine().split(",");
		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
		for (int i = 0; i < n; i++) {
			int xItem = Integer.parseInt(xItems[i]);
			x[i] = xItem;
		}
		int result = hackerlandRadioTransmitters(x, k);

		bufferedWriter.write(String.valueOf(result));
		bufferedWriter.newLine();

		bufferedWriter.close();

		scanner.close();
	}
}