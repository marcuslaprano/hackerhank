package br.marcus.hackerhank.datastructures.twodarray;

import java.util.Scanner;

public class Solution {

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		int[][] arr = new int[6][6];

		for (int i = 0; i < 6; i++) {
			String[] arrRowItems = scanner.nextLine().split(" ");
			scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

			for (int j = 0; j < 6; j++) {
				int arrItem = Integer.parseInt(arrRowItems[j]);
				arr[i][j] = arrItem;
			}
		}

		scanner.close();
		Integer index = 0;
		Integer greaterSum = null;
		while (index < 6) {
			try {
				int[] line1 = arr[index];
				int[] line2 = arr[index + 1];
				int[] line3 = arr[index + 2];

				for (int j = 0; j < 6; j++) {
					Integer first = line1[j] + line1[j + 1] + line1[j + 2] ;
					Integer second = line2[j + 1];
					Integer third = line3[j] + line3[j + 1] + line3[j + 2];
					Integer sum = first + second + third;
					if(greaterSum == null || sum > greaterSum) {
						greaterSum = sum;
					}
				}
			} catch (Exception e) {
				// TODO: handle exception
			}

			index++;
		}
		System.out.println(greaterSum);

	}
}