package br.marcus.hackerhank.datastructures.javalist;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int listLength = scan.nextInt();
		List<Integer> list = new ArrayList<>();
		for (int i = 0; i < listLength; i++) {
			list.add(scan.nextInt());
		}

		int queryNumbers = scan.nextInt();
		for (int i = 0; i < queryNumbers; i++) {
			String query = scan.next();
			if ("insert".equalsIgnoreCase(query)) {
				Integer position = scan.nextInt();
				Integer number = scan.nextInt();
				list.add(position, number);
			} else if ("delete".equalsIgnoreCase(query)) {
				Integer position = scan.nextInt();
				list.remove(list.get(position));
			}
		}
		for (Integer i : list) {
			System.out.print(i + " ");
		}
		scan.close();
	}

}
