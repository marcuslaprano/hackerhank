package br.marcus.hackerhank.datastructures.subarray;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Solution {
	
	private static final Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
			Integer length = scanner.nextInt();
			int[] arr = new int[length];
	
	        for(int k = 0; k < length; ++k){
	            arr[k] = scanner.nextInt();
	        }
			
			Integer index = 0;
			List<String> arrays = new ArrayList<>();
			while (index < length) {
				String valor =""; 
				for(int i = index; i < arr.length; i++) {
					valor = new String(valor+","+arr[i]);
					arrays.add(valor);
				}
				
				index++;
			}
			
			Integer sumTotal = 0;
			for(String value: arrays) {
				Integer total = 0;
				String[] arraysExploded = value.split(",");
				for(String valueExploded: arraysExploded) {
					if(!valueExploded.trim().isEmpty())
						total = new Integer(valueExploded) + total;
				}
				
				if(total < 0) {
					sumTotal++;
				}
				
			}
			
			System.out.println(sumTotal);
		
	}

}
