package br.marcus.hackerhank.datastructures.arraylist;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Solution {
	
private static final Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
			Integer length = scanner.nextInt();
			List<String> lista = new ArrayList<>();
			scanner.nextLine();
	        for(int k = 0; k < length; ++k){
	            lista.add(scanner.nextLine());
	        }
	        
	        Integer queryLength = scanner.nextInt();
	        String[] queries = new String[queryLength];
	        scanner.nextLine();
	        for(int k = 0; k < queryLength; ++k){
	            queries[k] = scanner.nextLine();
	        }
	        
	        for(int i = 0; i < queryLength; i++) {
	        	String[] positions = queries[i].split(" ");
	        	String[] positionValues = lista.get(Integer.parseInt(positions[0])-1).split(" ");
	        	try {
	        		System.out.println(positionValues[Integer.parseInt(positions[1])]);
				} catch (Exception e) {
					System.out.println("ERROR");
				}
	        }
	        
	        
	}

}
