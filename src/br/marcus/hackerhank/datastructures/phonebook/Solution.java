package br.marcus.hackerhank.datastructures.phonebook;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		in.nextLine();
		Map<String, Integer> mapa = new HashMap<String, Integer>();
		for (int i = 0; i < n; i++) {
			String name = in.nextLine();
			int phone = in.nextInt();
			mapa.put(name, phone);
			in.nextLine();
		}
		while (in.hasNext()) {
			String s = in.nextLine();
			if (mapa.get(s) == null) {
				System.out.println("Not found");
			} else {
				System.out.println(s + "=" + mapa.get(s));
			}

		}
		in.close();
	}

}
