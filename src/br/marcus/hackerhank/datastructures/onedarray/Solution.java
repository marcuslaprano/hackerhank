package br.marcus.hackerhank.datastructures.onedarray;

import java.util.*;

public class Solution {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		Integer[] a = new Integer[n];
		Integer index = 0;
		while(index < n) {
			a[index] = scan.nextInt();
			index++;
		}
		scan.close();
		for (int i = 0; i < a.length; i++) {
			System.out.println(a[i]);
		}
	}
}