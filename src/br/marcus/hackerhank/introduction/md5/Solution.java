package br.marcus.hackerhank.introduction.md5;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class Solution {
	public static void main(String[] args) {
		Scanner c = new Scanner(System.in);
		try {
			String m = c.nextLine();
			MessageDigest digest = MessageDigest.getInstance("MD5");
			digest.update(m.getBytes(), 0, m.length());
			byte[] m2 = digest.digest();
		    StringBuilder hex = new StringBuilder();
		    for (byte b : m2) {
		        hex.append(String.format("%02x", b));
		    }
		    System.out.println(hex.toString());
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally{
			c.close();
		}
	}
}
