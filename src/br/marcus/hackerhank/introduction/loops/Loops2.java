package br.marcus.hackerhank.introduction.loops;

import java.util.Scanner;

public class Loops2 {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int cursor = in.nextInt();
        for(int i=0 ; i< cursor; i++){
        	StringBuilder sb = new StringBuilder();
        	
        	int a = in.nextInt();
            int b = in.nextInt();
            int n = in.nextInt();
            int before = a;
            for (int z = 0; z < n; z++){
            	Double exp = Math.pow(2, z)*b;
            	Integer value = exp.intValue()+before;
            	before = value;
            	sb.append( value+" ");
            	
            }
            
            System.out.println(sb.toString());
            
        }
        in.close();
	}

}
