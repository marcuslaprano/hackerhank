package br.marcus.hackerhank.introduction.datatypes;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Datatypes {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();

		for (int i = 0; i < t; i++) {
			String value = sc.next();
			List<String> list = new ArrayList<>();
			String init = value + " can be fitted in:";
			list.add(init);
			byteMethod(list,value);
			shorMethod(list, value);
			intMethod(list, value);
			longMethod(list, value);
			
			if (list.size() == 1){
				System.out.println(value +" can't be fitted anywhere.");
			} else {
				for (String s: list){
					System.out.println(s);
				}
			}

		}
		sc.close();
	}

	private static void byteMethod(List<String> list, String value) {
		try {
			Byte.valueOf(value);
			list.add("* byte");
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

	private static void longMethod(List<String> list, String value) {
		try {
			Long.valueOf(value);
			list.add("* long");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private static void intMethod(List<String> list, String value) {
		try {
			Integer.valueOf(value);
			list.add("* int");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private static void shorMethod(List<String> list, String value){
		try {
			Short.valueOf(value);
			list.add("* short");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
