package br.marcus.hackerhank.introduction.datetime;

public class DateTimeHK {
	
	public static void main(String[] args) {
		System.out.println(getDay("05","08","2015"));
	}
	
	public static String getDay(String day, String month, String year) {
		java.util.Calendar cal = java.util.Calendar.getInstance();
		cal.set(java.util.Calendar.DATE, new Integer(day));
		cal.set(java.util.Calendar.MONTH, new Integer(month) - 1);
		cal.set(java.util.Calendar.YEAR, new Integer(year));
		return cal.getDisplayName(java.util.Calendar.DAY_OF_WEEK, java.util.Calendar.LONG, java.util.Locale.getDefault()).toUpperCase();
    }

}
