package br.marcus.hackerhank.introduction.endoffile;

import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Integer index = 0;
		while (scanner.hasNext()) {
			String s = scanner.nextLine();
			if (s.isEmpty()) {
				break;
			}
			System.out.println(++index + " " + s);

		}
		scanner.close();
	}

}
