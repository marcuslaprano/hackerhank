package br.marcus.hackerhank.strings.comparison;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String word = sc.nextLine();
        int len = sc.nextInt();
        
        List<String> list = new ArrayList<>();
        for(int k = 0; k <= word.length()-len; k++){
            String part = word.substring(k, len+k);
            list.add(part);
        }
        
        Collections.sort(list);
        System.out.println(list.get(0));
        System.out.println(list.get(list.size()-1));
        sc.close();
        
    }
}