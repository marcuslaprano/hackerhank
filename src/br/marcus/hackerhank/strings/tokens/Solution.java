package br.marcus.hackerhank.strings.tokens;

import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String s = scan.nextLine();
        s = s.replaceAll("[!,?._'@]+", " ");
        String[] tokens = s.split(" ");
        Integer totalTokens = 0;
        for(String token: tokens) {
        	if(!token.trim().isEmpty()) {
        		totalTokens++;
        	}
        }
        System.out.println(totalTokens);
        for(String token: tokens) {
        	if(!token.trim().isEmpty()) {
        		System.out.println(token);
        	}
        }
        scan.close();
    }
}
