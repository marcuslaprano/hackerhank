package br.marcus.hackerhank.strings.introduction;
import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        
        Scanner sc=new Scanner(System.in);
        String A=sc.next();
        String B=sc.next();
        int lA = A.length();
        int lB = B.length();
        System.out.println(lA+lB);
        System.out.println(A.compareTo(B) > 0?"Yes": "No");
        String cap = A.substring(0,1).toUpperCase().concat(A.substring(1,lA)).concat(" ").concat(B.substring(0,1).toUpperCase()).concat(B.substring(1,lB));
        System.out.println(cap);
        sc.close();
        
    }
}
