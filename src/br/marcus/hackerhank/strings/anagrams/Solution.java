package br.marcus.hackerhank.strings.anagrams;

import java.util.Arrays;
import java.util.Scanner;

public class Solution {
	
	static boolean isAnagram(String a, String b) {
		a = a.toLowerCase();
		b = b.toLowerCase();
        char[] lettersA = a.toCharArray();
        char[] lettersB = b.toCharArray();
        
        Arrays.sort(lettersA);
        Arrays.sort(lettersB);
        
        return Arrays.equals(lettersA, lettersB);
    }
	
	public static void main(String[] args) {
	    
        Scanner scan = new Scanner(System.in);
        String a = scan.next();
        String b = scan.next();
        scan.close();
        boolean ret = isAnagram(a, b);
        System.out.println( (ret) ? "Anagrams" : "Not Anagrams" );
    }

}
