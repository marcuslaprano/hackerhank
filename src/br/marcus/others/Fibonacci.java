package br.marcus.others;

public class Fibonacci {
	
	public static void main(String[] args) {
		fibonacci(12);
		for(int i = 0; i <= 12; i++) {
			System.out.print(recursiveFibonnacci(i)+" - ");
		}
	}
	
	public static int recursiveFibonnacci(int number) {
		if(number <= 1) {
			return number;
		}
		
		return recursiveFibonnacci(number-1)+recursiveFibonnacci(number-2);
	}
	
	public static void fibonacci(int max) {
		Long next = 0L;
		Long previous = 0L;
		Integer index = 0;
		while (index <= max) {
			if( index == 0) {
				next = 0L;
				previous = 0L;
			}
			if(index == 1) {
				next = 0L;
				previous = 1L;
			}
			next = next + previous;
			previous = next - previous;
			index++;
			System.out.println(next);
		}
	}

}
