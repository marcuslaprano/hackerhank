package br.marcus.others;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CountDistinctStrings {
	
	public static void main(String[] args) {
		List<String> lista = new ArrayList<>();
		lista.add("Marcus");
		lista.add("Tatiana");
		lista.add("Maria Eduarda");
		lista.add("Maria Terezinha");
		lista.add("Marcus");
		lista.add("Tatiana");
		lista.add("Maria Eduarda");
		lista.add("Maria Terezinha");
		lista.add("Marcus");
		lista.add("Tatiana");
		lista.add("Maria Eduarda");
		lista.add("Maria Terezinha");
		
		String[] A = lista.stream().toArray(size -> new String[size]);
		System.out.println(distinct(A));
		System.out.println(new HashSet<>(Stream.of(A).collect(Collectors.toList())).size());
		System.out.println(lista.stream().distinct().count());
		
	}
	
	private static int distinct(String[] A) {
		Set<String> distincts = new HashSet<>();
		for(String value: A) {
			distincts.add(value);
		}
		return distincts.size();
		
	}

}
