package br.marcus.others;

public class NeedleInAHaystack {
	
	public static void main(String[] args) {
		String needle = "marcus";
		String haystack = "eu tive que sair, me chamo marcus mazzo laprano. Minha m�e me deu o nome de marcus"
				+ "em referencia ao santo marcus. Marcus � um nome biblico";
		System.out.println(solution(needle, haystack));
				
	}
	
	private static int solution(String needle, String haystack) {
		if(needle != null && haystack != null) {
			if(needle.equals(" ")) {
				return haystack.replaceAll("[^\\s]", "").length();
			}
			
			return haystack.replaceAll(" ", "").replaceAll(needle, " ").replaceAll("[^\\s]", "").length();
			
		}
		return 0;
	}

}
