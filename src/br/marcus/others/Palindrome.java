package br.marcus.others;

import java.util.stream.IntStream;

public class Palindrome {
	
	public static void main(String[] args) {
		String pali = "naN";
		System.out.println(palindromeJava8(pali));
		System.out.println(palindrome(pali));
	}

	private static boolean palindromeJava8(String pali) {
		return IntStream.range(0, pali.length() / 2)
			      .noneMatch(i -> pali.toLowerCase().charAt(i) != pali.toLowerCase().charAt(pali.length() - i - 1));
	}
	
	private static boolean palindrome(String pali) {
		pali = pali.toLowerCase();
		for(int i = 0; i < pali.length() / 2; i++) {
			if(pali.charAt(i) != pali.charAt(pali.length()-i)) return false;
		}
		return true;
	}

}
