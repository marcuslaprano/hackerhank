package br.marcus.others;

public class Inheritance {

	public static void main(String[] args) {
		Father father = new Father();
		father = new Son();
		father.getBloodType();
		
		Son son = new Son();
		son.getColorEyes();

	}
}

class Father {
	public Father() {
		super();
	}

	private String colorEyes;
	private String skinColor;
	private String bloodType;

	public String getColorEyes() {
		return colorEyes;
	}

	public void setColorEyes(String colorEyes) {
		this.colorEyes = colorEyes;
	}

	public String getSkinColor() {
		return skinColor;
	}

	public void setSkinColor(String skinColor) {
		this.skinColor = skinColor;
	}

	public String getBloodType() {
		return bloodType;
	}

	public void setBloodType(String bloodType) {
		this.bloodType = bloodType;
	}
}

class Son extends Father {

	public Son() {
		super();
	}

	private String bloodType;

	public String getBloodType() {
		return bloodType;
	}

	public void setBloodType(String bloodType) {
		this.bloodType = bloodType;
	}

}
