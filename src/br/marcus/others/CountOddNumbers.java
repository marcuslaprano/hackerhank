package br.marcus.others;

public class CountOddNumbers {
	public static void main(String[] args) {
		int[] A = {1,2,3,4,5,6,7,8,9,10};
		System.out.println(counter(A));
	}
	
	public static long counter(int[] A) {
		Long total = 0L;
		for(Integer value: A) {
			if(value % 2 != 0) {
				total += value;
			}
		}
		return total;
	}


}
