package br.marcus.others;

public class Composition {
	public static void main(String[] args) {
		new Car(new Engine()).move(100);
	}
}

class Engine{
	
	public void move(Integer velocity) {
		System.out.println("moving at: "+velocity);
	}
}

class Car {
	
	private Engine engine;
	
	public Car(Engine engine) {
		this.engine = engine;
	}
	
	public void move(Integer velocity) {
		System.out.print("Car is ");
		this.engine.move(velocity);
	}
	
}
