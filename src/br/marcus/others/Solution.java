package br.marcus.others;

import java.util.Arrays;
import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Solution {
	
	public static void main(String[] args) {
		String needle = "marcus";
		String haystack = "eu tive que sair, me chamo marcus mazzo laprano. Minha m�e me deu o nome de marcus"
				+ "em referencia ao santo marcus. Marcus � um nome biblico ";
		System.out.println(needleInAHaystackReplace(needle, haystack));
		System.out.println(needleInAHaystackIndexOf(needle, haystack));
		fibonnaci(12);
		System.out.println(isPalindromeStringBuffer("anaa"));
		System.out.println(isPalindromeStringBuffer("ranara"));
		System.out.println(binaryGap(9));
		
		String[] A = {"ABC","ABC","ABC","ABC","ABC","ABC","CDE","CDE","CDE","CDE","CDE"};
		System.out.println(distinctCountStream(A));
		System.out.println(distinctCountHash(A));
		
		A = new String[]{"ABC","CDE","EDF","KDF","","KDF"};
		System.out.println(distinctCount(A));
	}
	
	
	public static int needleInAHaystackReplace(String needle, String haystack) {
		if(needle != null && haystack != null) {
			if(!haystack.contains(needle)) {
				return 0;
			}
			
			if (!needle.isEmpty() && !haystack.isEmpty()) {
				/*
				 *  verificando se o needle � semelhante ao caracter utilizando como
				 *  balizador para realizar os replaces necess�rios.  
				 */
				if(!needle.equals(" ")) {
					haystack = haystack.replaceAll(" ", "");
					haystack = haystack.replaceAll(needle, " ");
				}
				
				return haystack.replaceAll("[^\\s]", "").length();
				
			}
		}
		return 0;
	}
	
	public static int needleInAHaystackIndexOf(String needle, String haystack) {
		if(needle != null && haystack != null) {
			if(!haystack.contains(needle)) {
				return 0;
			}
			
			Integer index = 0;
			Integer total = 0;
			while(index < haystack.length()) {
				index = haystack.indexOf(needle, index);
				if(index < 0) {
					break;
				}
				
				index++;
				total++;
				
				
			}
			
			return total;
		}
		return 0;
	}
	
	
	public static void fibonnaci(Integer max) {
		Long ant = 0L;
		Long next = 0L;
		Integer index = 0;
		while(index < max) {
			if(index == 1) {
				ant = 1L;
			}
			
			next = next + ant;
			ant = next - ant;
			System.out.println("Fibo "+ index + " - " +next +" ");
			index++;
			
		}
		
	}
	
	public static boolean isPalindromeStringBuffer(String word) {
		if(word == null || word.isEmpty()) {
			return false;
		}
		
		StringBuffer bf = new StringBuffer();
		for(int i = word.length(); i > 0; i--) {
			bf.append(word.charAt(i-1));
		}
		
		return word.equals(bf.toString());
	}
	
	public static boolean isPalindrome(String word) {
		/* Por defini��o palindrome � uma palavra que lida da esqueda para direita � igual quando se l�
		 * da direita para a esquerda.
		 * 
		 * Utilizando essa defini��o temos os seguintes palindromes de exemplo:
		 * 	ana
		 * 	ranar
		 * 	aranara
		 *  raar
		 *  
		 * Utilizando essas palavras percebemos que, por defini��o, a primeira letra obrigat�riamente deve ser igual a �ltima letra
		 * assim como a segunda deve ser igual a pen�ltima, e assim por diante.
		 * O que nos leva a perceber que existe um padr�o:
		 * A metada inicial da palavra deve ser igual ao inverso da metade final da palavra
		 * 
		 * 	Exemplo
		 * 	raar = 	
		 * 			4 letras 
		 * 			metade inicial = ra
		 * 			metade final = ar
		 * 			metade final invertida = ra
		 * 
		 */
		
		for (int i = 0; i < word.length() / 2; i++) {
			if (word.charAt(i) != word.indexOf(word.length() - i)) return false;
		}
		
		return true;
		
	}
	
	public static int binaryGap(Integer number) {
		String binary = Integer.toBinaryString(number);
		binary = binary.replaceAll("$0+", "");
		String binaryArray [] = binary.split("1+");
		Arrays.sort(binaryArray);
		return binaryArray.length > 0 ? binaryArray[binaryArray.length-1].length(): 0;
	}
	
	
	public static long distinctCountStream(String[] A) {
		return Stream.of(A).collect(Collectors.toList()).stream().distinct().count();
	}
	
	public static long distinctCountHash(String[] A) {
		return new HashSet<>(Arrays.asList(A)).size();
	}
	
	public static long distinctCount(String[] A) {
		Arrays.sort(A);
		long total = A.length;
		for(int i = 1; i <= A.length; i++) {
			if (i == A.length) {
				break;
			}
			
			if(A[i] == A[i-1]) {
				total--;
			}
		}
		return total;
	}
	

}
