package br.marcus.others;

public class Aggregation {

	public static void main(String[] args) {
		CarA car = new CarA();
		car.setEngine(new EngineA());
		car.move(100);
	}
}

class EngineA {

	public void move(Integer velocity) {
		System.out.println("moving at: " + velocity);
	}
}

class CarA {

	private EngineA engine;

	public CarA() {
	}

	public void move(Integer velocity) {
		if(this.engine != null) {
			System.out.print("Car is ");
			this.engine.move(velocity);
		}
	}

	public EngineA getEngine() {
		return engine;
	}

	public void setEngine(EngineA engine) {
		this.engine = engine;
	}
	
	

}
