package br.marcus.codility.frogjump;

public class Solution {
    public int solution(int X, int Y, int D) {
	    	if(X >= Y) {
	    		return 0;
	    	}
	    	
	    	Integer valor = (Y-X)/D;
	    	return (valor * D) + X < Y ? valor+1:valor ;
    }
    
    public static void main(String[] args) {
		System.out.println(new Solution().solution(10, 85, 30));
	}

}
