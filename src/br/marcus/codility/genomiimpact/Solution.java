package br.marcus.codility.genomiimpact;

public class Solution {

	public int[] solution(String S, int[] P, int[] Q) {
		Integer M = 0;
		int[] range = new int[P.length];
		while(M < P.length) {
			Integer minimal = null;
			Integer sumC = 0, sumG = 0, sumT = 0;
			for(int i = 0; i < S.length(); i++) {
				if('A' == (S.charAt(i))) {
					minimal = 1;
					break;
				}
	            if ('C' == (S.charAt(i))) {
	            	sumC++;
					continue;
	            }
	            if ('G' == (S.charAt(i))) {
	            	sumG++;
					continue;
	            }
	            if ('T' == (S.charAt(i))) {
	            	sumT++;
					continue;
	            }
			}
			
			if(minimal == null) {
				
			}
			
			range[M] = minimal;
			M++;
		}
		return range;
	}
	
	public static void main(String[] args) {
		int [] P = {2};
		int [] Q = {4};
		int [] valor = new Solution().solution("CAGCCTA", P, Q);
		for(Integer val: valor) {
			System.out.print(val+" ");
		}
	}

}
