package br.marcus.codility.maxproducttree;

import java.util.Arrays;

public class Solution {
	
	public static void main(String[] args) {
		int[] A = {-3,1,2,-2,5,6};
		System.out.println(new Solution().solution(A));
	}
	
	public int solution(int[] A) {
		Arrays.sort(A);
		return Math.max(A[A.length-1]*A[A.length-2]*A[A.length-1], A[A.length-1]*A[0]*A[1]);
	}

}
