package br.marcus.codility.maxcounters;

public class Solution {

	public int[] solution(int N, int[] A) {
		int[] arr = new int[N];
		int maxValue = 0;
		int maxIndice = 0;
		for(int i = 0; i < A.length; i++) {
			int position = A[i];
			if(position == N+1) {
				maxValue=maxValue+1;
				maxIndice = i-1;
			} else {
				arr[position-1] = arr[position-1]+1;
			}
		}
		
		for(int i = 0; i < N; i++) {
			if(i < maxIndice) {
				arr[i] = arr[i]+maxValue;
			}
		}
		
		return arr;
	}
	
	public static void main(String[] args) {
		int A[] = {21,7,14,21,14,21,18,5,5,21,14,7,6,21,6,14,18,15,4,10,19,5,10,10,12,10,17,4,16,21};
		int[] arr = new Solution().solution(20, A);
		for(Integer value: arr) {
			System.out.print(value+" ");
		}
	}

}
