package br.marcus.codility.permcheck;

import java.util.Arrays;

class Solution {
	public int solution(int[] A) {
		Arrays.sort(A);
		int[] B = new int[A.length];
		for(int i = 1; i <= B.length; i++) {
			B[i-1] = i;
		}
		
		for (int i = 0; i < A.length; i++) {
			if (B[i] - A[i] != 0) {
				return 0;
			}
		}
		return 1;

	}
	
	public static void main(String[] args) {
		int[] A = {1,1};
		System.out.println(new Solution().solution(A));
	}
}