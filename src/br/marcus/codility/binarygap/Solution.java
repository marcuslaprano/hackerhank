package br.marcus.codility.binarygap;

import java.util.Date;
import java.util.stream.Stream;

public class Solution {
	public int solution(int N) {
		String binary = Integer.toBinaryString(N);
		binary = binary.replaceAll("0+$", "");
		String[] ones = binary.split("1+");
		Integer max = 0;
		for(String one: ones) {
			if(one.length() > max) {
				max = one.length();
			}
		}
		return max;
	}
	
	public int solutionJava8(int N) {
		return Stream.of(Integer.toBinaryString(N).replaceAll("0+$", "").split("1+")).filter(a -> a !=null).map(String::length).max(Integer::compare).orElse(0);
	}
	
	public static void main(String[] args) {
		Long init = new Date().getTime();
		System.out.println(new Solution().solution(15));
		
		Long end = new Date().getTime();
		System.out.println(init - end);
		
		init = new Date().getTime();
		System.out.println(new Solution().solutionJava8(15));
		end = new Date().getTime();
		
		System.out.println(init - end);
		
	}
}