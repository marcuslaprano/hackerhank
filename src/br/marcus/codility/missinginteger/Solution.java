package br.marcus.codility.missinginteger;

import java.util.HashSet;
import java.util.Set;

class Solution {
	public int solution(int[] A) {
		Set<Integer> set = new HashSet<Integer>();
		int min = 1;

		for (int i = 0; i < A.length; i++) {
			if (A[i] > 0) set.add(A[i]);
		}
		
		for(int i = 1 ; i < Integer.MAX_VALUE; i++) {
			if(!set.contains(i)) return i;
		}

		return min;
	}
	
	public static void main(String[] args) {
		int[] A = {1};
		System.out.println(new Solution().solution(A));
	}
}