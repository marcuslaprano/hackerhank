package br.marcus.codility.tapeequilibrium;

public class Solution {
	public int solution(int[] A) {
		
		int sum = 0;
		for(Integer num: A) {
			sum += num;
		}
		
		int min = Integer.MAX_VALUE;
		int left = A[0];
		int right = sum-left;

		for (int i = 1; i < A.length; i++) {
			int diff = Math.abs(left - right); 
			if (min > diff) {
				min = diff;
			}
			left = left + A[i];
			right = sum - left;
		}

		return min;
	}

	public static void main(String[] args) {
		int[] A = { 3, 1, 2, 4, 3 };
		System.out.println(new Solution().solution(A));
	}
}