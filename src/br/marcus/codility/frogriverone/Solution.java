package br.marcus.codility.frogriverone;

import java.util.HashSet;
import java.util.Set;

class Solution {
    public int solution(int X, int[] A) {
    	Set<Integer> sets = new HashSet<>();
    	
    	int sumSet = 0;
    	for(int i = 1; i <= X; i++) {
    		sets.add(i);
    		sumSet += i;
    	}
    	
    	if(!sets.contains(1)) {
    		return -1;
    	}
    	
    	if(sumSet == 0) {
    		return -1;
    	}
    	
        int total = 0;
        Set<Integer> aparicoes = new HashSet<>();
        int position = -1;
        
        for(int i = 0; i < A.length; i++){
        	if (!aparicoes.contains(A[i])) {
        		total += A[i];
        		aparicoes.add(A[i]);
        	}
        	
        	if(total == sumSet) {
        		position = i;
        		break;
        	}
        }
        
        return position;
    }	
    
    
    public static void main(String[] args) {
    	int[] array = {3};
		System.out.println(new Solution().solution(3,array));
	}
}