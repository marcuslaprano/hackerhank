package br.marcus.codility.passingcars;

public class Solution {
	public int solution(int[] A) {
		
		if(A == null || A.length ==0) {
			return 0;
		}
		
		int multiplicador = 0;
		int total = 0;
		for(int i = 0; i < A.length; i++) {
			if (A[i] == 0) {
				multiplicador++;
			} else {
				if (multiplicador > 0 && A[i] == 1) {
					total = total+multiplicador;
					if(total > 1000000000) {
						return -1;
					}
				}
			}
			
		}
		
		return total;
	}
	
	public static void main(String[] args) {
		int[] A = {0,0,0,1};
		System.out.println(new Solution().solution(A));
		
		int[] B = {0,1,0,1,1};
		System.out.println(new Solution().solution(B));
	}
}
