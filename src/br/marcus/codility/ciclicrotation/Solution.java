package br.marcus.codility.ciclicrotation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Solution {

	public int[] solution(int[] A, int K) {
		List<Integer> lista = new ArrayList<>();
		for(int i = 0; i < A.length; i++) {
			lista.add(A[i]);
		}
		
		Collections.rotate(lista, K);
		for(int i = 0; i < lista.size(); i++) {
			A[i] = lista.get(i);
		}
		return A;
	}
	
	public int[] solutionJava8(int[] A, int K) {
		List<Integer> list = Arrays.stream(A).boxed().collect(Collectors.toList());
		Collections.rotate(list, K);
		return list.stream().mapToInt(i->i).toArray();
	}
	
	public static void main(String[] args) {
		int[] lista = {3,8,9,7,6};
		Long init = new Date().getTime();
		for(Integer a: new Solution().solutionJava8(lista, 3)){
			System.out.print(a + " ");
		}
		Long end = new Date().getTime();
		System.out.println(end-init);
		
		init = new Date().getTime();
		for(Integer a: new Solution().solution(lista, 3)){
			System.out.print(a + " ");
		}
		end = new Date().getTime();
		System.out.println(end-init);
	}

}
