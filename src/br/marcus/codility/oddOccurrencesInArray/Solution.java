package br.marcus.codility.oddOccurrencesInArray;

public class Solution {
	
	public int solution(int[] A) {
        Integer umpaired = 0;
        for(Integer i : A){
            umpaired ^= i;
        }
        
        return umpaired;
        
    }
	
	public static void main(String[] args) {
		int[] arr = {1,1,2,2,3,4,4};
		System.out.println(new Solution().solution(arr));
	}

}
