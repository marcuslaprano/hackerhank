package br.marcus.codility.permmissingelem;

import java.util.Arrays;

public class Solution {
	public int solution(int[] A) {
		if (A == null || Arrays.asList(A).isEmpty() || A.length == 0) {
			return 1;
		}

		Arrays.sort(A);
		if (A[0] != 1) {
			return 1;
		}

		for (int i = 0; i < A.length; i++) {
			if (i + 1 <= A.length - 1)
				if (A[i + 1] - A[i] > 1) {
					return A[i] + 1;
				}
		}

		return A[A.length - 1] + 1;

	}

}
